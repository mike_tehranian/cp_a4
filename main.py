
import cv2
import numpy as np
import scipy as sp
import math

from os import path

import assignment4 as a4

"""
You can use this file to write code for part 2. You are NOT required to use
this file, and ARE ALLOWED to make ANY changes you want in THIS file. This file
MAY be submitted with your report as an "additional file" in the submit script
(see the "-f" option). If you write code for above & beyond effort, make sure
that you include important snippets in your writeup, because SUBMITTING YOUR
CODE ALONE IS NOT SUFFICIENT FOR ABOVE AND BEYOND CREDIT.

    DO NOT SHARE CODE (INCLUDING TEST CASES) WITH OTHER STUDENTS.
"""

IMG_FOLDER = "test_images"

IMAGE_TEST = np.array([[ 230, 0, 235, 0, 240],
                       [ 0, 241, 0, 242, 0],
                       [ 243, 0, 244, 0, 245],
                       [ 0, 246, 0, 247, 0],
                       [ 248, 0, 249, 0, 250]], dtype=np.uint8)

GAUSSIAN_KERNEL = np.array([[  1,  4,  6,  4,  1],
                            [  4, 16, 24, 16,  4],
                            [  6, 24, 36, 24,  6],
                            [  4, 16, 24, 16,  4],
                            [  1,  4,  6,  4,  1]], dtype=np.float64) / 256.

BOX_KERNEL = np.array([[ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                       [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                       [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                       [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                       [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                       [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                       [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                       [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                       [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                       [ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]], dtype=np.float64) / 100.

BOX_KERNEL_3x3 = np.array([[ 1, 1, 1],
                           [ 1, 1, 1],
                           [ 1, 1, 1]], dtype=np.float64) / 9.

PREWITT_X = np.array([[ -1, 0, 1],
                      [ -1, 0, 1],
                      [ -1, 0, 1]], dtype=np.float64)

PREWITT_Y = np.array([[ -1, -1, -1],
                       [ 0, 0, 0],
                       [ 1, 1, 1]], dtype=np.float64)

SOBEL_X = np.array([[ -1, 0, 1],
                    [ -2, 0, 2],
                    [ -1, 0, 1]], dtype=np.float64)

SOBEL_Y = np.array([[ -1, -2, -1],
                     [ 0, 0, 0],
                     [ 1, 2, 1]], dtype=np.float64)


def find_edges(image):
    image_edge = np.zeros(image.shape, dtype=np.int64)
    number_of_rows = image.shape[0]
    number_of_columns = image.shape[1]

    gradient_x = sobel_x(image)
    gradient_y = sobel_y(image)
    # gradient_x = prewitt_x(image)
    # gradient_y = prewitt_y(image)

    for row in range(number_of_rows):
        for column in range(number_of_columns):
            image_edge[row, column] = int(math.sqrt(gradient_x[row, column]**2 \
                    + gradient_y[row, column]**2))
    return image_edge

def convert_to_black_and_white(image):
    black_and_white_image = np.zeros(image.shape, dtype=np.uint8)
    for index, value in np.ndenumerate(image):
        if value > 80:
            black_and_white_image[index] = 255
        else:
            black_and_white_image[index] = 0
    return black_and_white_image

def main():
    img = cv2.imread(path.join(IMG_FOLDER, "baybridge.jpg"), cv2.IMREAD_GRAYSCALE)
    # g_image = gaussian_noise_reduction(img)
    # kernel = GAUSSIAN_KERNEL
    # image = a4.pyFilter2D(img, kernel)
    # cv2.imwrite("perf_test2.png", g_image)

    # canny_edge_detector(img)
    # edge_img = gaussian_noise_reduction(img)
    # edge_img = find_edges(img)
    # norm_img = a4.normalizeImage(edge_img)
    # bw_img = convert_to_black_and_white(norm_img)
    # cv2.imwrite("final_edge_img_sobel_80.png", bw_img)

def canny_edge_detector(image):
    sigma = 0.75
    v = np.median(image)
    lower = int(max(0, (1.0 - sigma) * v))
    upper = int(min(255, (1.0 + sigma) * v))
    edged = cv2.Canny(image, lower, upper)
    cv2.imwrite("final_edge_img_canny.png", edged)

def gaussian_noise_reduction(image):
    kernel = GAUSSIAN_KERNEL
    gaussian_image = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0, borderType=cv2.BORDER_REFLECT_101)
    # gaussian_image = a4.pyFilter2D(image, kernel)
    return gaussian_image

def prewitt_x(image):
    kernel = PREWITT_X
    prewitt_x = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   borderType=cv2.BORDER_REFLECT_101)
    # prewitt_x = a4.pyFilter2D(image, kernel)
    return prewitt_x

def prewitt_y(image):
    kernel = PREWITT_Y
    prewitt_y = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   borderType=cv2.BORDER_REFLECT_101)
    # prewitt_x = a4.pyFilter2D(image, kernel)
    return prewitt_y

def sobel_x(image):
    kernel = SOBEL_X
    sobel_x = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   borderType=cv2.BORDER_REFLECT_101)
    # prewitt_x = a4.pyFilter2D(image, kernel)
    return sobel_x

def sobel_y(image):
    kernel = SOBEL_Y
    sobel_y = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   borderType=cv2.BORDER_REFLECT_101)
    # prewitt_x = a4.pyFilter2D(image, kernel)
    return sobel_y

def part2filters():
    image = cv2.imread(path.join(IMG_FOLDER, "baybridge.jpg"), cv2.IMREAD_GRAYSCALE)

    # cv_image = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   # borderType=cv2.BORDER_REFLECT_101)

    kernel = BOX_KERNEL
    # box_image = a4.pyFilter2D(image, kernel)
    box_image = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   borderType=cv2.BORDER_REFLECT_101)
    cv2.imwrite("baybridge_box.png", box_image)

    kernel = GAUSSIAN_KERNEL
    gaussian_image = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0, borderType=cv2.BORDER_REFLECT_101)
    # gaussian_image = a4.pyFilter2D(image, kernel)
    cv2.imwrite("baybridge_gaussian.png", gaussian_image)

    kernel = PREWITT_X
    prewitt_x = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   borderType=cv2.BORDER_REFLECT_101)
    # prewitt_x = a4.pyFilter2D(image, kernel)
    cv2.imwrite("baybridge_prewitt_x.png", prewitt_x)

    kernel = PREWITT_Y
    prewitt_y = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   borderType=cv2.BORDER_REFLECT_101)
    # prewitt_y = a4.pyFilter2D(image, kernel)
    cv2.imwrite("baybridge_prewitt_y.png", prewitt_y)

    kernel = SOBEL_X
    sobel_x = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   borderType=cv2.BORDER_REFLECT_101)
    # prewitt_x = a4.pyFilter2D(image, kernel)
    cv2.imwrite("baybridge_sobel_x.png", sobel_x)

    kernel = SOBEL_Y
    sobel_y = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   borderType=cv2.BORDER_REFLECT_101)
    # prewitt_y = a4.pyFilter2D(image, kernel)
    cv2.imwrite("baybridge_sobel_y.png", sobel_y)

def testgradient():
    m = np.array([[ 0, 1, 2, 3, 4],
    [ 5, 6, 7, 8, 9],
    [10, 11, 12, 13, 14],
    [15, 16, 17, 18, 19]])

    import ipdb; ipdb.set_trace()
    gradient_x = a4.normalizeImage(IMAGE_TEST)

    import ipdb; ipdb.set_trace()
    print(gradient_x)

def testpadding():
    m = np.array([[ 0, 1, 2, 3, 4],
                  [ 5, 6, 7, 8, 9],
                  [10, 11, 12, 13, 14],
                  [15, 16, 17, 18, 19]])

    image = cv2.imread(path.join(IMG_FOLDER, "baybridge.jpg"), cv2.IMREAD_GRAYSCALE)
    n = cv2.copyMakeBorder(image,1000,1000,1000,1000,cv2.BORDER_REFLECT_101)
    cv2.imwrite("mypaddingbridge.png", n)
    # n = a4.padReflectBorder(image, 1000)
    # cv2.imwrite("mypaddingbridge.png", n)

    # print(ref)
    # print(n)

def testxcorrelation():
    cv2.imwrite("cv_image_me_test.png", IMAGE_TEST)
    image = cv2.imread(path.join(IMG_FOLDER, "butterfly.jpg"),
            cv2.IMREAD_GRAYSCALE)
    kernel = BOX_KERNEL_3x3
    cv_image = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   borderType=cv2.BORDER_REFLECT_101)
    # filtered_image = a4.pyFilter2D(image, kernel)
    filtered_image = a4.pyFilter2D(image, kernel)
    # MDT try normalizing here, don't think it's actually needed
    # norm_img = a4.normalizeImage(edge_img)
    cv2.imwrite("cv_image_test.png", cv_image)
    cv2.imwrite("cv_image_me_test_final.png", filtered_image)
    filtered_image_norm = a4.normalizeImage(filtered_image)
    cv2.imwrite("cv_image_me_test_final_norm.png", filtered_image_norm)

def changeimagetogray():
    image = cv2.imread(path.join(IMG_FOLDER, "baybridge.jpg"),
            cv2.IMREAD_GRAYSCALE)
    cv2.imwrite("baybridgegray.png", image)


if __name__ == "__main__":
    image = cv2.imread(path.join(IMG_FOLDER, "baybridge.jpg"),
        cv2.IMREAD_GRAYSCALE)
    kernel = BOX_KERNEL
    box_image = cv2.filter2D(image, cv2.CV_16S, kernel, anchor=(-1,-1), delta=0,
                   borderType=cv2.BORDER_REFLECT_101)
    cv2.imwrite("baybridge_bigger_box.png", box_image)

    # testpadding()
    # testxcorrelation()
    # testgradient()
    # part2filters()
    # main()
    # changeimagetogray()
    # image = cv2.imread(path.join(IMG_FOLDER, "butterfly.jpg"),
            # cv2.IMREAD_GRAYSCALE)
    # x_img = a4.gradientX(image)
    # cv2.imwrite("x_imgbutter.png", a4.normalizeImage(x_img))
    # y_img = a4.gradientY(image)
    # cv2.imwrite("y_imgbutter.png", a4.normalizeImage(y_img))
    pass

